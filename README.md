# Opened Eyes



## Task

Дана неразмеченная выборка изображений закрытых и открытых глаз, требуется построить модель, которая на выход будет выдавать действительное число от 0.0 до 1.0, где 0 означает, что глаз закрыт, а 1, что открыт. Особенность задачи заключается в том, что данные не размечены.
